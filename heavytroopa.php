<?php
    if(empty($_GET["password"]) || $_GET["password"] != "PASS") {
        die();
    }

    function buildQuery($sql, $filters) {
        $wheres = [];

        foreach($filters as $type => $query) {
            switch($type) {
                case "platform":
                    $wheres[] = "`platform` = :platform";
                    break;

                case "submitter":
                    $wheres[] = "LOWER(`submitter`) LIKE LOWER(:submitter)";
                    break;

                case "title":
                    $wheres[] = "LOWER(`title`) LIKE LOWER(:title)";
                    break;
            }
        }

        if (count($wheres) > 0) {
            $sql .= " WHERE ";
        }

        $sql .= implode(" AND ", $wheres);

        return $sql;
    }

    function bindParams($dbh, $filters) {
        foreach($filters as $type => $query) {
            switch($type) {
                case "platform":
                    $dbh->bindParam(":platform", $filters["platform"], PDO::PARAM_INT);
                    break;

                case "submitter":
                    $dbh->bindParam(":submitter", $filters["submitter"], PDO::PARAM_STR);
                    break;

                case "title":
                    $filters["title"] = "%" . $filters["title"] . "%";
                    $dbh->bindParam(":title", $filters["title"], PDO::PARAM_STR);
                    break;
            }
        }
    }

    function randomOfficial($filters) {
        $dbh = new PDO('mysql:host=localhost;dbname=heavytroopa', "USER", "PASS");

        // resolve platform filter
        if (!empty($filters["platform"])) {
            $query = $dbh->prepare("SELECT platform FROM `platform_aliases` WHERE LOWER(`alias`)=LOWER(:alias)");
            $query->bindParam(":alias", $filters["platform"], PDO::PARAM_STR);
            $query->execute();
            $result = $query->fetchAll(PDO::FETCH_COLUMN);
            if(empty($result)) {
                return ["error" => true, "reason" => "That's not a platform I know about."];
            }
            $platform_id = (int)$result[0];


            $filters["platform"] = $platform_id;
        }

        $sql = buildQuery("SELECT count(*) FROM `games`", $filters);

        $query = $dbh->prepare($sql);
        bindParams($query, $filters);

        $result = $query->execute();

        $result = $query->fetchAll(PDO::FETCH_COLUMN);
        $number_of_rows = $result[0];

        if ($number_of_rows == 0) {
            return ["error" => true, "reason" => "I tried really hard, but couldn't find any games for what you asked."];
        }

        $id = rand(0, $number_of_rows-1);

        $sql = buildQuery("SELECT * FROM `games`", $filters);
        $sql .= " LIMIT :id,1";

        $query = $dbh->prepare($sql);

        bindParams($query, $filters);
        $query->bindParam(":id", $id, PDO::PARAM_INT);

        $query->execute();

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result[0];
    }

    function jsonRandomOfficial($filters = array()) {
        $game = randomOfficial($filters);

        print(json_encode($game));
    }

    jsonRandomOfficial($_GET["filter"]);