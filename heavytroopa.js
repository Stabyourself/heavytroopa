const keys = require("./keys");
var fs = require("fs");
var util = require("util");
var logFile = fs.createWriteStream("log.txt", { flags: "a" });
// Or 'w' to truncate the file every time the process starts.
var logStdout = process.stdout;

console.log = function () {
  logFile.write(util.format.apply(null, arguments) + "\n");
  logStdout.write(util.format.apply(null, arguments) + "\n");
};
console.error = console.log;

const platforms = require("./platforms");
var GiantBomb = require("giant-bomb");
var gb = new GiantBomb(keys.gbdbKey, "MFH randomizer bot");

const Discord = require("discord.js");
const discordClient = new Discord.Client();
var getJSON = require("get-json");
var request = require("request").defaults({ encoding: null });

function getNick(message) {
  if (message.member) {
    return message.member.displayName;
  } else {
    return "Blind racer";
  }
}

function getRandomSRLGame(message) {
  getJSON("https://api.speedrunslive.com/games")
    .then(function (response) {
      const games = response.games;
      const randomIndex = Math.floor(Math.random() * games.length);
      const game = response.games[randomIndex];

      const imgUrl =
        "https://cdn.speedrunslive.com/images/games/" + game.abbrev + ".jpg";

      var options = {};

      request.get(imgUrl, function (err, res, buffer) {
        if (res.statusCode === 200) {
          const attachment = new Discord.MessageAttachment();
          attachment.setFile(buffer);
          attachment.setName(game.abbrev + ".jpg");
          options.file = attachment;
        }

        message.channel.send(
          getNick(message) +
            ', your new speedgame is "' +
            game.name +
            '". Have fun! <https://www.speedrunslive.com/races/game/#!/' +
            game.abbrev +
            "/1>",
          options
        );
      });
    })
    .catch(function (error) {
      message.channel.send("Something went wrong, sorry :(");
      console.log(error);
    });
}

function getOfficialGame(message) {
  var url = "https://stuff.guegan.de/heavytroopa.php?password=readytolaunch";

  var query = message.content.slice(10);
  query = query.replace("=", ":"); // Make both filter:query and filter=query work

  if (query.length > 0) {
    var split = query.split(",");

    var filters = [];

    for (var i = 0; i < split.length; i++) {
      var split2 = split[i].trim().split(":");

      if (split2.length == 2) {
        if (split2[0] == "console") {
          // make "console" work the same as (the actually correct) "platform"
          split2[0] = "platform";
        }
        filters.push({
          name: split2[0].trim(),
          value: split2[1].trim(),
        });
      }
    }

    for (var i = 0; i < filters.length; i++) {
      url += `&filter[${filters[i].name}]=${filters[i].value}`;
    }
  }

  // message.channel.send(url)
  getJSON(url)
    .then(function (response) {
      const game = response;

      if (game.error) {
        message.channel.send(game.reason);
        return;
      }

      const out = `
            Title: ${game.title}
            Platform: ${game.console}
            Goal: ${game.goal}
            Estimate: ${game.estimate}
            URL (may be dead): ${game.url}
            Pastebin: <${game.notes}>
        `;

      message.channel.send(out);
    })
    .catch(function (error) {
      message.channel.send("Something went wrong, sorry :(");
      console.log(error);
    });
}

function getGBDBGame(message) {
  var tourney = false;
  var tourney5 = false;

  var filterParameter = message.content.slice(5).trim(); // remove !gbdb, allows !gbdbtourney as well as !gbdb tourney

  if (filterParameter.substring(0, 8).toLowerCase() == "tourney5") {
    tourney = true;
    tourney5 = true;

    filterParameter = filterParameter.slice(8).trim(); // remove tourney
  }

  if (filterParameter.substring(0, 7).toLowerCase() == "tourney") {
    tourney = true;

    filterParameter = filterParameter.slice(7).trim(); // remove tourney
  }

  var count = 1;

  if (tourney5) {
    count = 5;
  }

  function nextGame() {
    if (count > 0) {
      count--;

      var options = {
        fields: ["id"],
        limit: 1,
      };

      var filterPlatforms = null;
      if (tourney) {
        filterPlatforms = [
          [31, 79, 80, 119],
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          14,
          16,
          18,
          19,
          21,
          22,
          23,
          29,
          36,
          42,
          43,
          52,
          53,
          54,
          55,
          57,
          65,
          81,
          84,
          91,
          94,
          140,
        ];
      }

      if (filterParameter.length > 0 && !tourney) {
        // only allow filtering outside of tourney
        var platformSearchResult = platforms.findPlatform(filterParameter);

        if (platformSearchResult) {
          filterPlatforms = [platformSearchResult.id];
        }
      }

      // new GBDBTourney filter rule: filter by platform before getting a random game
      if (tourney) {
        var platform = filterPlatforms;

        while (Array.isArray(platform)) {
          // resolve down until we're at a number
          platform = platform[Math.floor(Math.random() * platform.length)];
        }

        filterPlatforms = [platform]; // but filter is an array so put it in one
      }

      if (filterPlatforms) {
        options.filter = "platforms:" + filterPlatforms.join("|");
      }

      gb.getGames(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
          var offset = Math.floor(Math.random() * body.number_of_total_results);

          // adjust options for getting the final game
          options.offset = offset;

          gb.getGames(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
              if (!body.results[0]) {
                console.log(body);
                GBDBFail("Uuuuhhhh");
                return;
              }
              options.id = body.results[0].id;
              delete options.offset;
              options.fields = [
                "name",
                "platforms",
                "genres",
                "developers",
                "site_detail_url",
                "image",
                "original_release_date",
                "deck",
                "images",
              ];

              gb.getGame(options, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                  sendGBDBResult(message, body.results, filterPlatforms);
                  nextGame();
                } else {
                  GBDBFail(error);
                }
              });
            } else {
              GBDBFail(error);
            }
          });
        } else {
          GBDBFail(error);
        }
      });
    }
  }

  nextGame();
}

const GBDBFooters = [
  "A special game, just for %user%.",
  "Not sure if this is something %user% really wants to play.",
  "%user% loves this kind of game.",
  "Doesn't %user% have the wr in this?",
  "Why did you pull this, %user%?",
  "This is %user%'s fault.",
  "Actually a good game, thanks %user%.",
  "What kind of game is this, %user%? I'd rather play Mr. Nutz...",
  "%user% may as well submit this to kuso.",
  "%user% will forfeit this.",
  "%user% submitted this 13 times to MT already.",
  "%user% made this.",
  "Not sure if %user%'s PC can run this.",
  "This may be the only game in which %user% can be beaten.",
  "No redraws, %user%.",
  "This footer only exists to show that it was %user% who drew this.",
  "I think this is the perfect game for you, %user%.",
];

function sendGBDBResult(message, game, filterPlatforms) {
  // platform of choice needs to be: What player selected OR within the tourney filter OR random
  if (game.platforms) {
    var possiblePlatforms = game.platforms.slice();

    if (filterPlatforms) {
      // remove all platforms not in filterPlatforms
      var i = possiblePlatforms.length;
      while (i--) {
        if (filterPlatforms.indexOf(possiblePlatforms[i].id) === -1) {
          possiblePlatforms.splice(i, 1);
        }
      }
    }

    // select a random platform from what's left
    var platformOfChoice =
      possiblePlatforms[Math.floor(Math.random() * possiblePlatforms.length)]
        .name;

    // prepare platforms
    game.platforms = game.platforms.map((v) => {
      return v.name;
    });
  } else {
    game.platforms = ["Unknown"];
    platformOfChoice = "Unknown";
  }

  if (game.developers) {
    game.developers = game.developers.map((v) => {
      return v.name;
    });
  } else {
    game.developers = ["Unknown"];
  }

  if (game.genres) {
    game.genres = game.genres.map((v) => {
      return v.name;
    });
  } else {
    game.genres = ["Unknown"];
  }

  if (game.original_release_date) {
    game.original_release_date = game.original_release_date.split(" ")[0]; // split off the time
  } else {
    game.original_release_date = "Unknown";
  }

  var description = "";
  description +=
    "**Platform" +
    (game.platforms.length != 1 ? "s" : "") +
    ":** " +
    game.platforms.join(", ");
  description += "\n**Platform of choice:** " + platformOfChoice;
  description +=
    "\n**Developer" +
    (game.developers.length != 1 ? "s" : "") +
    ":** " +
    game.developers.join(", ");
  description +=
    "\n**Genre" +
    (game.genres.length != 1 ? "s" : "") +
    ":** " +
    game.genres.join(", ");
  description += "\n**Released:** " + game.original_release_date;
  description += "\n\n" + (game.deck ? game.deck : "???");
  description += "\n[Giantbomb page](" + game.site_detail_url + ")";

  var footer = GBDBFooters[Math.floor(Math.random() * GBDBFooters.length)];
  footer = footer.replace("%user%", getNick(message));

  const embed = new Discord.MessageEmbed()
    .setAuthor(game.name)
    .setColor([247, 247, 39])
    .setThumbnail(game.image.original_url)

    .setDescription(description);
  // .setFooter(footer)

  message.channel.send({ embed });
}

function GBDBFail(error) {
  console.log(error);
  message.channel.send("Something went wrong, sorry!");
}

function toggleRole(message, roleName, emote) {
  if (!message.member) {
    message.channel.send(
      "You're either invisible or something else went wrong, sorry!"
    );
    return;
  }

  emote = emote || ":sparkles:";

  var member = message.member;
  var role = message.guild.roles.cache.find((r) => r.name === roleName);

  if (message.member.roles.cache.has(role.id)) {
    member.roles.remove(role).catch(console.error);
    message.channel.send(
      emote +
        " You no longer have the " +
        roleName +
        " role, " +
        getNick(message) +
        "."
    );
  } else {
    member.roles.add(role).catch(console.error);
    message.channel.send(
      emote +
        " You now have the " +
        roleName +
        " role, " +
        getNick(message) +
        "."
    );
  }
}

function pasteBin(message) {
  message.channel.send("<https://pastebin.com/ty3CGwcP>");
}

discordClient.on("ready", () => {
  console.log("heavytroopa running");
});

const gameChannels = [
  "509423665314791456",
  "430142080359202817",
  "271770880399507456",
]; // test server, heavytroopa-toruney, blind-o-matic
const gbdbChannels = [
  "509423665314791456",
  "271770880399507456",
  "648262455025467402",
  "647166828879151138",
  "704234172574728232",
]; // test server, blind-o-matic, game-nights
const roleChannels = ["509423665314791456", "271770880399507456"]; // test server, blind-o-matic
const officialChannels = ["509423665314791456", "271770880399507456"]; // test server, blind-o-matic
const pasteBinChannels = [
  "509423665314791456",
  "430142080359202817",
  "271770880399507456",
]; // test server, heavytroopa-toruney, blind-o-matic

discordClient.on("message", (message) => {
  if (message.content.startsWith("!game")) {
    if (gameChannels.indexOf(message.channel.id) != -1) {
      getRandomSRLGame(message);
    }
  }

  if (message.content.startsWith("!gbdb")) {
    if (gbdbChannels.indexOf(message.channel.id) != -1) {
      getGBDBGame(message);
    }
  }

  if (message.content.startsWith("!shobango")) {
    if (roleChannels.indexOf(message.channel.id) != -1) {
      toggleRole(message, "Shobango");
    }
  }

  if (message.content.startsWith("!metroid")) {
    if (roleChannels.indexOf(message.channel.id) != -1) {
      toggleRole(message, "Metroid Nerds");
    }
  }

  if (message.content.startsWith("!pingme")) {
    if (roleChannels.indexOf(message.channel.id) != -1) {
      toggleRole(message, "MFHNotify", ":ping_pong:");
    }
  }

  if (message.content.startsWith("!amongus")) {
    if (roleChannels.indexOf(message.channel.id) != -1) {
      toggleRole(message, "us, among", "<:itisamystery:280870150729629696>");
    }
  }

  if (message.content.startsWith("!shmup")) {
    if (roleChannels.indexOf(message.channel.id) != -1) {
      toggleRole(message, "1ccrew", ":airplane_small:");
    }
  }

  if (message.content.startsWith("!grandpricks")) {
    if (roleChannels.indexOf(message.channel.id) != -1) {
      toggleRole(message, "grand pricks", ":race_car:");
    }
  }

  if (message.content.startsWith("!flashmob")) {
    if (roleChannels.indexOf(message.channel.id) != -1) {
      toggleRole(message, "flash mob", ":camera_with_flash:");
    }
  }

  if (message.content.startsWith("!bored")) {
    if (roleChannels.indexOf(message.channel.id) != -1) {
      toggleRole(message, "BoredGamer", "<:angy:774342761641279549>");
    }
  }

  if (message.content.startsWith("!boints")) {
    if (roleChannels.indexOf(message.channel.id) != -1) {
      toggleRole(message, "BigBointer", ":eggplant:");
    }
  }

  if (message.content.startsWith("!event1")) {
    if (roleChannels.indexOf(message.channel.id) != -1) {
      toggleRole(message, "Event 1", ":one:");
    }
  }

  if (message.content.startsWith("!event2")) {
    if (roleChannels.indexOf(message.channel.id) != -1) {
      toggleRole(message, "Event 2", ":two:");
    }
  }

  if (message.content.startsWith("!puzzlehunter")) {
    if (roleChannels.indexOf(message.channel.id) != -1) {
      toggleRole(
        message,
        "puzzle hunter",
        "<:itisajollymania:777580926132682772>"
      );
    }
  }

  if (message.content.startsWith("!givegame")) {
    if (roleChannels.indexOf(message.channel.id) != -1) {
      toggleRole(message, "GIVEGAME", "<a:do_it:845905193018064908>");
    }
  }

  if (message.content.startsWith("!rejectgame")) {
    if (roleChannels.indexOf(message.channel.id) != -1) {
      toggleRole(message, "REJECTGAME", "<:TF2John:801576698017349642>");
    }
  }

  if (message.content.startsWith("!official")) {
    if (officialChannels.indexOf(message.channel.id) != -1) {
      getOfficialGame(message);
    }
  }

  if (message.content.startsWith("!pastebin")) {
    if (pasteBinChannels.indexOf(message.channel.id) != -1) {
      pasteBin(message);
    }
  }

  if (message.content.startsWith("!teapartycthulu")) {
    if (gbdbChannels.indexOf(message.channel.id) != -1) {
      message.content = "!gbdb saturn";
      getGBDBGame(message);
    }
  }

  if (message.content.startsWith("!flip")) {
    if (gbdbChannels.indexOf(message.channel.id) != -1) {
      if (Math.random() >= 0.5) {
        message.channel.send("You ban first!");
      } else {
        message.channel.send("The other person bans first!");
      }
    }
  }

  if (message.content.startsWith("!hexchat")) {
    message.channel.send(
      "Hexchat setup guide: https://www.youtube.com/watch?v=72JZgmErL7w"
    );
  }

  if (message.content.startsWith("!help")) {
    if (gbdbChannels.indexOf(message.channel.id) != -1) {
      message.channel.send(`Cool commands:
!game — Get a random game from SRL
!gbdb [tourney(5)?] [platform?] — Get a random game from the Giantbomb API
!official — Get a random game from past MTs
!flip — Decide who bans first

Check <#430141328668491776> for self-assignable roles!`);
    }
  }
});

discordClient.login(keys.botId);
